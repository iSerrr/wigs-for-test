const getStorage = (key) => {
    window.localStorage.getItem(JSON.parse(key))
}

const removeStorage = (key) => {
    window.localStorage.removeItem(JSON.parse(key))
}

const setStorage = (key, data) => {
    window.localStorage.setItem(key, JSON.stringify(data))
}

export { setStorage, getStorage, removeStorage }
