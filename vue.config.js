module.exports = {
    devServer: {
        proxy: 'http://localhost:82',
    },
    css: {
        loaderOptions: {
            scss: {
                additionalData: `@import "~@/styles/global.scss";`,
            },
        },
    },
}
